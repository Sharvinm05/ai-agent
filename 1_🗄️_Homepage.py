import sys
import time
import streamlit as st
from crewai import Agent, Task, Crew, Process
from langchain_community.tools import DuckDuckGoSearchRun
from langchain.agents import Tool
from langchain_openai import ChatOpenAI
import os
import re
from langchain_core.messages import HumanMessage, SystemMessage
import streamlit as st
import streamlit_authenticator as stauth
import yaml
from yaml.loader import SafeLoader
from dotenv import load_dotenv

# Load the .env file
load_dotenv()

# Access the API key value
api_key = os.getenv('API_KEY')

API_KEY = api_key
duckduckgo_search = DuckDuckGoSearchRun()
a = 0
task_values = []

def app_login():
    
    with open('./config.yaml') as file:
        config = yaml.load(file, Loader=SafeLoader)

    authenticator = stauth.Authenticate(
        config['credentials'],
        config['cookie']['name'],
        config['cookie']['key'],
        config['cookie']['expiry_days'],
        config['pre-authorized']
    )
    authenticator.login()
    print(st.session_state["authentication_status"])
    if st.session_state["authentication_status"] is True:
        
        # st.write(f'Welcome *{st.session_state["name"]}*')
        run_crewai_app()
        # st.title('Some content')
    elif st.session_state["authentication_status"] is False:
        st.error('Username/password is incorrect')
    elif st.session_state["authentication_status"] is None:
        st.warning('Please enter your username and password')

def create_crewai_setup(title,topic,accuracy,roleplay):
    
    llm = ChatOpenAI(
      model='gpt-3.5-turbo',
      api_key=API_KEY,
      temperature=accuracy,
      max_tokens=4000,
    )
    
    messages = [
    SystemMessage(
        content=f"Act as {roleplay} and explain in a professional way."
    ),
    HumanMessage(
        content=f"Customer that is eager to learn new knowledge."
    ),
    ]
    llm.invoke(messages)

    # Define Agents
    researcher = Agent(
        role="Researcher",
        goal=f"Analyze, discorver and study on {title}: {topic}.",
        backstory=f"Expert at understanding technology, target audience, and tools that support the topic: {topic}. Skilled in conveying the key features to reach a wide audience.",
        verbose=True,
        allow_delegation=True,
        tools=[duckduckgo_search],
        llm=llm,
    )

    writer = Agent(
        role="Technical Writer Expert",
        goal=f"Write a high-quality technical report on {title}: {topic}",
        backstory=f"You're {roleplay} in creating technical report. Write the technical report with minimum 5000 words",
        verbose=True,
        allow_delegation=True,
        llm=llm,
    )

    # business_consultant = Agent(
    #     role="Business Development Consultant",
    #     goal=f"Evaluate the business model for {topic}, focusing on scalability and revenue streams",
    #     backstory=f"Seasoned in shaping business strategies for products like {topic}. Understands scalability and potential revenue streams to ensure long-term sustainability.",
    #     verbose=True,
    #     allow_delegation=True,
    #     llm=llm,
    # )

    # Define Tasks

    task1 = Task(
    description=f"Assess and analyze every aspect of {title}: {topic}. Identify it's purpose, target audience, and key features.",
    expected_output="Complete analyzed content on introduction, purpose, target audience, key features and executive summary.",
    agent=researcher,
)

    task2 = Task(
        description=f"Write a technical report to reach the widest possible audience on {title}: {topic}.",
        expected_output=f"Technical report format on a detailed introduction, purpose, complete procedures and executive summary. Act as you're {roleplay}",
        agent=writer,
    )

    

    # task3 = Task(
    #     description=f"Summarize the market and technological reports and evaluate the business model for {topic}. Write a report on the scalability and revenue streams for the product. Include at least 10 bullet points on key business areas. Give Business Plan, Goals and Timeline for the product launch. Current month is Jan 2024.",
    #     expected_output="Report on business model evaluation and product launch plan.",
    #     agent=business_consultant,
    # )

    # Create and Run the Crew
    product_crew = Crew(
        agents=[researcher,writer],
        tasks=[task1,task2],
        verbose=2,
        process=Process.sequential,
    )

    crew_result = product_crew.kickoff()
    return crew_result


#display the console processing on streamlit UI
class StreamToExpander:
    def __init__(self, expander):
        self.expander = expander
        self.buffer = []
        self.colors = ['red', 'green', 'blue', 'orange']  # Define a list of colors
        self.color_index = 0  # Initialize color index

    def write(self, data):
        # Filter out ANSI escape codes using a regular expression
        cleaned_data = re.sub(r'\x1B\[[0-9;]*[mK]', '', data)

        # Check if the data contains 'task' information
        task_match_object = re.search(r'\"task\"\s*:\s*\"(.*?)\"', cleaned_data, re.IGNORECASE)
        task_match_input = re.search(r'task\s*:\s*([^\n]*)', cleaned_data, re.IGNORECASE)
        task_value = None
        if task_match_object:
            task_value = task_match_object.group(1)
        elif task_match_input:
            task_value = task_match_input.group(1).strip()

        if task_value:
            st.toast(":robot_face: " + task_value)

        # Check if the text contains the specified phrase and apply color
        if "Entering new CrewAgentExecutor chain" in cleaned_data:
            # Apply different color and switch color index
            self.color_index = (self.color_index + 1) % len(self.colors)  # Increment color index and wrap around if necessary

            cleaned_data = cleaned_data.replace("Entering new CrewAgentExecutor chain", f":{self.colors[self.color_index]}[Entering new CrewAgentExecutor chain]")

        if "Researcher" in cleaned_data:
            # Apply different color 
            cleaned_data = cleaned_data.replace("Researcher", f":{self.colors[self.color_index]}[Researcher]")
        if "Technical Writer Expert" in cleaned_data:
            cleaned_data = cleaned_data.replace("Technical Writer Expert", f":{self.colors[self.color_index]}[Technical Writer Expert]")
        # if "Technology Expert" in cleaned_data:
        #     cleaned_data = cleaned_data.replace("Technology Expert", f":{self.colors[self.color_index]}[Technology Expert]")
        if "Finished chain." in cleaned_data:
            cleaned_data = cleaned_data.replace("Finished chain.", f":{self.colors[self.color_index]}[Finished chain.]")

        self.buffer.append(cleaned_data)
        if "\n" in data:
            self.expander.markdown(''.join(self.buffer), unsafe_allow_html=True)
            self.buffer = []

# Streamlit interface
def run_crewai_app():
    st.set_page_config(
    page_title="AI Agent - Technical Writer",
    page_icon="🤖",
    )
    st.header("🤖 AI Agent - Technical Writer", divider='blue')
    
    with st.expander("About the Team:"):
        st.subheader("Diagram")
        left_co, cent_co,last_co = st.columns(3)
        with cent_co:
            st.image("my_img.png")

        st.subheader("Researcher & Analyst")
        st.text("""       
        Role = Researcher & Analyst
        Goal = Assess and analyze every aspect of {title}: {topic}. 
               Identify it's purpose, target audience, and key features.
        Backstory = Expert at understanding technology, target audience, 
                    and tools that support the topic: {topic}. 
                    Skilled in conveying the key features to reach a wide audience.
        Task = Assess and analyze every aspect of {title}: {topic}. 
               Identify it's purpose, target audience, and key features. """)
        
        st.subheader("Technical Writer Expert")
        st.text("""       
        Role = Technical Writer Expert
        Goal = Write a high-quality technical report on {title}: {topic}
        Backstory = You're an expert in creating technical report. 
                    Write the technical report with minimum 5000 words.
        Task = Write a technical report to reach the widest possible audience on {title}: {topic}. 
               Technical report format on a detailed introduction, purpose, 
               complete procedures and executive summary. """)

        st.subheader("Reviewer Expert")
        st.text("""       
        Role = Reviewer Expert 
        Goal= Evaluate the technical report and contents for {topic}
              focusing on Strengths and Areas for Improvement.
        Backstory = Seasoned in shaping quality review like {topic}. 
                    Understands scalability and potential 
                    revenue streams to ensure long-term sustainability.
        Task = Write a technical report to reach the widest possible audience on {topic}. 
               Technical report format on a detailed introduction, 
               purpose, complete procedures and executive summary. """)
    st.markdown("<br>", unsafe_allow_html=True)
    title = st.text_input("Title:",placeholder="Enter the title here...",max_chars=30)
    topic = st.text_area(
    "Enter the topic or details to analyze the information and produce report.",
    placeholder="Enter the brief information here...", 
    height=100
    )
    st.write(f'You wrote {len(topic)} characters.')
    role = st.selectbox("Select the role", ["Researcher & Analyst", "Reviewer Expert","Technical Writer Expert"])
    roleplay = st.selectbox("Roleplay as: ", ["Peer Review", "Elon Musk", "Joe Rogan of Crypto","Carl Sagan of Crypto"])
    accuracy = st.slider("Select the accuracy level", 0.0, 1.0, 0.5, 0.05)
    if "my_input" not in st.session_state:
        st.session_state["my_input"] = ""

    if st.button("Run Analysis"):
        # Placeholder for stopwatch
        stopwatch_placeholder = st.empty()
        
        # Start the stopwatch
        start_time = time.time()
        with st.status("Processing...", expanded=True) as status:
            sys.stdout = StreamToExpander(st)
            with st.spinner("Generating Results"):
                crew_result = create_crewai_setup(title,topic,accuracy,roleplay)
            status.update(label="Completed!", state="complete", expanded=False)

        # Stop the stopwatch
        end_time = time.time()
        total_time = end_time - start_time
        stopwatch_placeholder.text(f"Total Time Elapsed: {total_time:.2f} seconds")

        # st.header("Tasks:")
        # st.table({"Tasks" : task_values})
        
        st.header("Results:")
        
        my_input =crew_result
        st.session_state["my_input"] = my_input
        st.markdown(my_input)



if __name__ == "__main__":
    # app_login()
    run_crewai_app()