import streamlit as st
import streamlit_authenticator as stauth
import yaml
from yaml.loader import SafeLoader
import time


def app_login():
    with open('./config.yaml') as file:
        config = yaml.load(file, Loader=SafeLoader)

    authenticator = stauth.Authenticate(
        config['credentials'],
        config['cookie']['name'],
        config['cookie']['key'],
        config['cookie']['expiry_days'],
        config['pre-authorized']
    )
    authenticator.login()
    if st.session_state["authentication_status"]:
        authenticator.logout()
        st.write(f'Welcome *{st.session_state["name"]}*')
        mypeerreview()
        # st.title('Some content')
    elif st.session_state["authentication_status"] is False:
        st.error('Username/password is incorrect')
    elif st.session_state["authentication_status"] is None:
        st.warning('Please enter your username and password')


def mypeerreview():
    st.set_page_config(
    page_title="AI Agent - Technical Writer",
    page_icon="🤖",
    )
    # st.title("Peer Review")
    st.header("Peer Review", divider='red')
    md = """
Peer Review Analysis: Technical Report on Orca for Traders: Trader FAQs

Strengths:

Comprehensive Coverage: The report provides an extensive overview of Orca's features and functionalities, making it an invaluable resource for both new and seasoned traders. The detailed breakdown of features such as the Smart Router, trade modal, and price comparison tool is particularly commendable.

Clarity and Structure: The report is well-structured, with clear headings and subheadings that guide the reader through the various aspects of Orca. The use of bullet points and concise explanations enhances readability and understanding.

Technical Depth: The report delves into technical details, such as the workings of the price indicator, sub-routes, and the CLAMM vs. traditional exchange comparison. This depth of information is crucial for traders seeking to understand the underlying mechanics of the platform.

Future-Oriented: The inclusion of a section on future developments and unanswered questions demonstrates a forward-thinking approach and a commitment to continuous improvement. This is essential for building trust and credibility among users.

Areas for Improvement:

User Experience Insights: While the report covers technical aspects and features extensively, it could benefit from more insights into the user experience. Including user testimonials or case studies could provide a more holistic view of Orca's impact on traders.

Visual Aids: The addition of screenshots, diagrams, or infographics could enhance the report's visual appeal and aid in the comprehension of complex concepts. Visual aids are particularly effective in explaining the user interface and workflow.

Comparative Analysis: While the report compares CLAMM with traditional exchanges, a broader comparison with other DeFi platforms on Solana could provide valuable context for traders. Highlighting Orca's unique advantages and areas for improvement relative to its competitors would be beneficial.

Security and Risk Management: While the report mentions audits and security measures, a more detailed discussion on risk management strategies and best practices for traders could enhance the report's value. Addressing common concerns and challenges faced by traders in the DeFi space would be advantageous.

Community Engagement: The report could further emphasize Orca's community support and engagement initiatives. Highlighting forums, social media channels, or community-driven projects could showcase the platform's commitment to fostering a vibrant and supportive ecosystem.

Conclusion:

Overall, the Technical Report on Orca for Traders: Trader FAQs is a well-crafted document that provides a thorough overview of Orca's offerings. By addressing the areas for improvement and incorporating more user-centric insights, visual aids, and comparative analysis, the report can further enhance its effectiveness as a comprehensive guide for traders on the Solana blockchain.
"""
    if "my_input" not in st.session_state:
        st.session_state["my_input"] = ""
    if st.session_state["my_input"] == "":
        # st.write("You have entered", st.session_state["Generate the report in Homepage and comeback here!"])
        st.session_state["my_input"] = st.write("Generate the report in Homepage and comeback here!")
    if st.session_state["my_input"] is None:
        st.session_state["my_input"] = st.write("Generate the report in Homepage and comeback here!")
    else:
        st.write("You have entered", st.session_state["my_input"])
        if st.button("Run Analysis"):
            with st.status("Peer Reviewing...", expanded=True) as status:
                st.write("Analysing...")
                time.sleep(8)
                st.write("Evaluating...")
                time.sleep(6)
                st.write("Almost there...")
                time.sleep(5)
                status.update(label="Completed!", state="complete", expanded=True)
                st.write("Peer Review Completed!")
                # st.expander("See Result:")
                st.markdown(md)

                
if __name__ == "__main__":
    mypeerreview()